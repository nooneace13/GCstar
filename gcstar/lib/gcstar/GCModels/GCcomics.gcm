<?xml version="1.0" encoding="UTF-8"?>
<collection xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:noNamespaceSchemaLocation="https://gitlab.com/GCstar/GCstar/raw/Test/gcstar/share/gcstar/schemas/gcm-revision5.xsd"
name="GCcomics">
	<lang>GCcomics</lang>
	<options>
		<defaults>
			<image>no.png</image>
		</defaults>

		<fields>
			<id>id</id>
			<title>name</title>
			<cover>image</cover>
			<url>webPage</url>
			<search>
				<field>series</field>
				<field>writer</field>
				<field>title</field>
			</search>
			<results>
				<field>series</field>
				<field>volume</field>
				<field>title</field>
                <field>writer</field>
				<field>type</field>
				<field>format</field>
				<field>publishdate</field>
                <field>publisher</field>
			</results>
		</fields>

		<overlay>
            <image>subtle.png</image>
			<paddingLeft>12</paddingLeft>
			<paddingRight>11</paddingRight>
			<paddingTop>12</paddingTop>
			<paddingBottom>0</paddingBottom>
		</overlay>

		<values id="signing">
			<value displayed="SigningNo">0</value>
			<value displayed="SigningYes">1</value>
		</values>
		<values id="favouriteYesno">
			<value displayed="PanelFavourite">1</value>
			<value displayed="">0</value>
		</values>
	</options>

	
	<groups>
		<group id="main" label="Main"/>
		<group id="info" label="General"/>
		<group id="details" label="Details"/>
	</groups>

	<random>
	</random>

	<fields lending="true" tags="true">
		<field value="id"
				type="number"
				label="Id"
				init=""
				group="main"
				imported="false"/>
		<field value="name"
				group="main"
				init="%series% #%volume[%03d]% %title%"
				label="Name"
				imported="true"
				type="formatted" />
		<field value="series"
				type="history text"
				label="Series"
				init="New Comic"
				group="main"
				imported="true"/>
		<field value="volume"
				type="number"
				label="Volume"
				init="1"		
				min="0"
				max="9999999"
				group="main"
				imported="true"/>				
		<field value="title"
				type="short text"
				label="Title"
				init=""
				group="main"
				imported="true"/>
		<field value="writer"
				type="history text"
				label="Writer"
				init=""
				group="info"
				imported="true"/>
		<field value="illustrator"
				type="history text"
				label="Illustrator"
				init=""
				group="info"
				imported="true"/>
		<field value="colourist"
				type="history text"
				label="Colourist"
				init=""
				group="info"
				imported="true"/>
                <field value="inker"
                                type="history text"
                                label="Inker"
                                init=""
                                group="info"
                                imported="true"/>
                <field value="artist"
                                type="history text"
                                label="Artist"
                                init=""
                                group="info"
                                imported="true"/>
                <field value="letterer"
                                type="history text"
                                label="Letterer"
                                init=""
                                group="info"
                                imported="true"/>
		<field value="publisher"
				type="history text"
				label="Publisher"
				init=""
				group="info"
				imported="true"/>
		<field value="synopsis"
				type="long text"
				label="Synopsis"
				init=""
				group="info"
				imported="true"/>
		<field value="collection"
				type="history text"
				label="Collection"
				init=""
				group="info"
				imported="true"/>
		<field value="publishdate"
				type="date"
				label="PublishDate"
				init=""
				group="details"
				imported="true"/>
		<field value="printdate"
				type="date"
				label="PrintingDate"
				init=""
				group="details"
				imported="true"/>
		<field value="image"
				type="image"
				label="Image"
				init=""
				group="info"
				imported="true"/>
		<field value="backpic"
				type="image"
				label="ContextImgBack"
				init=""
				group="info"
				linkedto="image"
				imported="true"/>
		<field value="webPage"
				type="button"
				format="url"
				label="Url"
				init=""
				group="main"
				imported="true"/>
		<field value="added"
				type="date"
				label="PanelAdded"
				init="current"
				group="details"
				imported="false"/>
		<field value="isbn"
				type="short text"
				label="ISBN"
				init=""
				group="details"
				imported="true"/>
		<field value="type"
				type="history text"
				label="Type"
				init=""
				group="details"
				imported="true"/>
		<field value="category"
				type="history text"
				label="Category"
				init=""
				group="details"
				imported="true"/>
		<field value="format"
				type="history text"
				label="Format"
				init=""
				group="details"
				imported="true"/>
		<field value="numberboards"
				type="number"
				label="NumberBoards"
				init="1"
				min="0"
				max="9999999"
				group="details"
				imported="true"/>
		<field value="signing"
				type="yesno"
				label="Signing"
				init="0"
				group="details"
				notnull="true"
				imported="false"/>
		<field value="cost"
				type="number"
				label="Cost"
				init=""
				group="details"
				imported="true"/>
		<field value="rating"
				type="number"
                displayas="graphical"
				label="Rating"
				init="0"
				max="10"
				group="details"
				imported="true"/>
		<field value="comment"
				type="long text"
				label="Comment"
				init=""
				group="details"
				imported="true"/>
		<field value="file"
				type="file"
				format="simple"
				label="File"
				init=""
				group="details"
				imported="false"/>
	</fields>

	
	<filters>
		<group label="General">
			<filter field="series" comparison="contain" quick="true"/>
			<filter field="writer" comparison="contain" quick="true"/>
			<filter field="illustrator" comparison="contain" quick="true"/>
			<filter field="colourist" comparison="contain" quick="true"/>
			<filter field="publisher" comparison="contain" quick="true"/>
			<filter field="publishdate" comparison="range" numeric="true" preprocess="extractYear"/>
			<filter field="printdate" comparison="range" numeric="true" preprocess="extractYear"/>
		</group>
		<group label="Details">
			<filter field="rating" comparison="ge" numeric="true" labelselect="FilterRatingSelect" quick="true"/>
		</group>
		<group label="PanelLending">
			<filter field="borrower" comparison="eq" quick="true"/>
		</group>
	</filters>


	<panels>
		<panel name="form" label="PanelForm" editable="true">
			<item type="line">
				<item type="value" for="id" width="5" nomargin="true" />
				<item type="label" for="series" nomargin="true" />
				<item type="value" for="series" expand="true" nomargin="true" />
				<item type="label" for="volume" nomargin="true" />
				<item type="value" for="volume" nomargin="true" />					
				<item type="special" for="searchButton" nomargin="true" />
				<item type="special" for="refreshButton" nomargin="true" />
			
			</item>
			<item type="notebook" expand="true">
				<item type="tab" value="info" title="General">
					<item type="line">
						<item type="value" for="image" width="130" height="170"/>
						<item type="table" rows="5" cols="4" expand="true">
							<item type="label" for="title" row="0" col="0"/>
							<item type="value" for="title" row="0" col="1" colspan="3"/>
							<item type="label" for="writer" row="1" col="0"/>
							<item type="value" for="writer" row="1" col="1" colspan="1"/>
                                                        <item type="label" for="artist" row="1" col="2"/>
                                                        <item type="value" for="artist" row="1" col="3" colspan="1"/>
							<item type="label" for="illustrator" row="2" col="0"/>
							<item type="value" for="illustrator" row="2" col="1" colspan="1"/>
                                                        <item type="label" for="inker" row="2" col="2"/>
                                                        <item type="value" for="inker" row="2" col="3" colspan="1"/>
							<item type="label" for="colourist" row="3" col="0"/>
							<item type="value" for="colourist" row="3" col="1" colspan="1"/>
                                                        <item type="label" for="letterer" row="3" col="2"/>
                                                        <item type="value" for="letterer" row="3" col="3" colspan="1"/>
							<item type="label" for="publisher" row="4" col="0"/>
							<item type="value" for="publisher" row="4" col="1" colspan="1"/>
                                                        <item type="label" for="collection" row="4" col="2"/>
                                                        <item type="value" for="collection" row="4" col="3" colspan="1"/>
 						</item>
					</item>
					<item type="line" expand="true">
						<item type="value" for="synopsis" expand="true"/>
					</item>
				</item>
				<item type="tab" value="details" title="Details">
				    <item type="line">
    					<item type="table" rows="6" cols="4" expand="true">
                            <item row="0" col="0" type="label" for="rating"/>
                            <item row="0" col="1" type="value" for="rating" colspan="2"/>
                            <item row="1" col="0" type="label" for="type" />
                            <item row="0" col="3" type="value" for="signing" colspan="1"/>
    						<item row="1" col="1" type="value" for="type"/>
    						<item row="1" col="2" type="label" for="category"/>
    						<item row="1" col="3" type="value" for="category"/>
    						<item row="2" col="0" type="label" for="publishdate"/>
    						<item row="2" col="1" type="value" for="publishdate"/>
    						<item row="2" col="2" type="label" for="printdate"/>
    						<item row="2" col="3" type="value" for="printdate"/>
    						<item row="3" col="0" type="label" for="added"/>
    						<item row="3" col="1" type="value" for="added"/>
    						<item row="3" col="2" type="label" for="cost"/>
    						<item row="3" col="3" type="value" for="cost"/>
    						<item row="4" col="0" type="label" for="isbn"/>
    						<item row="4" col="1" type="value" for="isbn"/>
    						<item row="4" col="2" type="label" for="format"/>
    						<item row="4" col="3" type="value" for="format"/>
    						<item row="5" col="0" type="label" for="numberboards"/>
    						<item row="5" col="1" type="value" for="numberboards"/>
                            <item row="6" col="0" type="label" for="file"/>
                            <item row="6" col="1" type="line" colspan="3">
        						<item type="value" for="file" expand="true"/>
    					       	<item type="launcher" for="file"/>
    					    </item>
                        </item>
                    </item>
					<item type="line" expand="true">
						<item type="label" for="comment"/>
						<item type="value" for="comment" expand="true"/>
					</item>
				</item>
				<item type="tab" value="lending" title="PanelLending">
					<item type="table" rows="3" cols="3">
						<item type="label" for="borrower" row="0" col="0"/>
						<item type="value" for="borrower" row="0" col="1"/>
						<item type="special" for="mailButton" row="0" col="2"/>
						<item type="label" for="lendDate" row="1" col="0"/>
						<item type="value" for="lendDate" row="1" col="1"/>
						<item type="special" for="itemBackButton" row="1" col="2"/>
					</item>
					<item type="label" for="borrowings" align="left"/>
					<item type="line" expand="true">
						<item type="box" width="64"/>
						<item type="value" for="borrowings" expand="true"/>
						<item type="box" width="64"/>
					</item>
				</item>
				<item type="tab" value="tagpanel" title="PanelTags">
					<item type="line">
						<item type="value" for="favourite" />
					</item>
					<item expand="true" for="tags" type="value" />
				</item>
			</item>
			<item type="line" homogeneous="true">
				<item type="value" for="webPage" expand="true"/>
				<item type="special" for="deleteButton" expand="true"/>
			</item>
		</panel>
	</panels>

</collection>
